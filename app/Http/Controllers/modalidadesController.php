<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;

class modalidadesController extends Controller
{
    //

    public function getTodas()
    {
    	$modalidades = Modalidad::all();
    	return view('modalidades.index', array("Modalidades" => $modalidades));
    }

    public function getVer($id)
	{    	
		$modalidad = Modalidad::findOrFail($id);
		$participantes = Participante::where('modalidad_id', $id)->get();
		return view('modalidades.mostrar', array('Modalidad' => $modalidad, 'Participantes' => $participantes));
	}
}
