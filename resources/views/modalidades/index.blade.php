@extends('layouts.master')
@section('titulo')
Cantabria Skills
@endsection
@section('contenido')
{{-- <div class="container"> --}}
	<div class="row">
		@foreach($Modalidades as $clave => $modalidad)
		<div style="border:1px solid black;" class="col-xs-6 col-sm-1 col-md-4">
			<img src="assets/imagenes/modalidades/{{$modalidad->imagen}}" class='fluid' class="card-img-bottom" height="90" width="90" margin='10' />
				<a href="{{ url('/modalidades/mostrar/' . $modalidad->id ) }}">
					<h4 style="min-height:45px;margin:5px 0 10px 0">
						{{$modalidad->nombre}}
					</h4>
				</a>							
		</div>
		@endforeach
	</div>
{{-- </div> --}}
@endsection