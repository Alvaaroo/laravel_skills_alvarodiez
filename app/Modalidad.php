<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modalidad extends Model
{
    //
	protected $table = 'modalidades';
	protected $primaryKey = 'id';
	public $timestamps = false;	

}