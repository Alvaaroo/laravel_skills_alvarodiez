@extends('layouts.master')
@section('titulo')
mostrar Modalidad 
@endsection
@section('contenido')
<div class="row">	
	<div class="col-sm-12">
		{{-- TODO: Datos de la mascota --}}
		<h2 style="min-height:45px;margin:5px 0 10px 0">{{$Modalidad->nombre}}</h2>		
		<h5>Familia profesional:</h5>
		<p>{{$Modalidad->familiaProfesional}}</p>
		<h5>Participantes</h5>	
		<div class="col-xs-6 col-sm-1 col-md-4">
			@foreach($Participantes as $participante)		
			<p>{{$participante->nombre}}</p>
			<img src="assets/imagenes/participantes/{{$participante->imagen}}" class="fluid" height="80" width="80">			
			@endforeach
		</div>
		<a class="btn btn-warning" href="#">Editar</a>
		<a class="btn btn-light" href="#">Volver al listado</a>
	</div>
</div>
{{-- @endif --}}
{{-- @endforeach --}}
@endsection