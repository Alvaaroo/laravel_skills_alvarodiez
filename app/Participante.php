<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participante extends Model
{
    //
	protected $table = 'participantes';
	protected $primaryKey = 'id';
	public $timestamps = false;	
}
